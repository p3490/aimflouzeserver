from classifier.classifier import Classifier
from keras.preprocessing.image import img_to_array
from typing import Any, List, Tuple
from keras.models import load_model
import pickle
import cv2
import numpy as np
from image_utils.picture_utils import get_image_size


class ModelClassifier(Classifier[str, Any]):
    """
    This classifier uses a trained model to classify images.
    His keys corresponds to the label of this classifier.
    His samples are images.
    """

    def __init__(self, model_path: str, labels_path: str, image_dimension: Tuple[int, int, int]):
        self.model = load_model(model_path)
        file = open(labels_path, "rb")
        self.labels: List[str] = pickle.loads(file.read()).classes_
        file.close()
        self.image_dimensions = image_dimension

    # Overrides the abstract method from Classifier
    def guess(self, sample: Any) -> List[float]:
        if min(get_image_size(sample)) == 0:
            return [0] * len(self.get_label())
        image = cv2.resize(sample, self.image_dimensions[0:2])
        image = image.astype("float") / 255.0
        image = img_to_array(image)
        image = np.expand_dims(image, axis=0)
        result = list(self.model.predict(image)[0])
        result = list(map(lambda e: float(e), result))
        return result

    # Overrides the abstract method from Classifier
    def get_label(self) -> List[str]:
        return list(self.labels)
