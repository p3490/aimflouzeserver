from abc import ABCMeta

from typing import List,  Dict, TypeVar
from .classifier import Classifier

K = TypeVar('K')
S = TypeVar('S')


class MultipleDictBasedClassifier(Classifier[K, S], metaclass=ABCMeta):
    """
    This is a abstract class to implement Classifier based mainly on the function
    guess_multiples_with_labels. It cas be useful in cases the classifier works better
    with multiples values.
    """

    # Overrides the method of Classifier
    def guess(self, sample: S) -> List[float]:
        return self.guess_multiples([sample])[0]

    # Overrides the method of Classifier
    def guess_with_labels(self, sample: S) -> Dict[K, float]:
        return self.guess_multiples_with_labels([sample])[0]

    # Overrides the method of Classifier
    def guess_multiples(self, samples: List[S]) -> List[List[float]]:
        labels = self.get_label()
        dict_result = self.guess_multiples_with_labels(samples)
        list_result = []
        for i in range(0, len(dict_result)):
            list_result.append([])
            for label in labels:
                list_result[i].append(dict_result[i][label])
        return list_result

    # Overrides the method of Classifier
    def guess_multiples_with_labels(self, samples: List[S]) -> List[Dict[K, float]]:
        raise NotImplementedError()
