import abc
from typing import List, Generic, TypeVar, Dict, Tuple
K = TypeVar('K')
S = TypeVar('S')


class Classifier(Generic[K, S], metaclass=abc.ABCMeta):
    """
    This abstract class represent a classifier.
    A classofier job is to classify a data. For each of his labels, it gives a peercentage of certitude between 0 and 1
    """

    @abc.abstractmethod
    def guess(self, sample: S) -> List[float]:
        """
        This method recieves an sample to classify. It returns a list containing each peercentage of probability to be
        each label given by get_label, in the same order.

        :param sample: The sample to test
        :return: The list of each peercentage (Between 0 and 1)
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def get_label(self) -> List[K]:
        """
        Return the list of label that this classifier user.

        :return: The list of label used
        """
        raise NotImplementedError()

    def guess_with_labels(self, sample: S) -> Dict[K, float]:
        """
        This method recieves an sample to classify. It returns a dictionary associating each label to a probability
        that the sample is of this label.

        :param sample: The sample to test
        :return: A dictionary associating each label to a probability that the sample is of this label (Between 0 and 1)
        """
        labels = self.get_label()
        result_list = self.guess(sample)
        result: Dict[str, float] = {}
        for index in range(len(labels)):
            result[labels[index]] = result_list[index]
        return result

    def guess_multiples(self, samples: List[S]) -> List[List[float]]:
        """
        This method recieves a list of samples to classify. It returns a list of list containing each peercentage of
        probability to be each label given by get_label, in the same order.

        :param samples: A list of samples to test
        :return: The list of each peercentage (between 0 and 1)
        """
        result: List[List[float]] = []
        for sample in samples:
            result.append(self.guess(sample))
        return result

    def guess_multiples_with_labels(self, samples: List[S]) -> List[Dict[K, float]]:
        """
        This method recieves a list of samples to classify. It returns a list of dictionary associating each label to
        a probability that the sample is of this label.

        :param samples: A list of samples to test
        :return: A list of dictionaries associating each label to a probability that the sample is of this label
                 (Between 0 and 1)
        """
        labels = self.get_label()
        guess_results = self.guess_multiples(samples)
        result: List[Dict[str, float]] = []
        for guess_result in guess_results:
            to_add: Dict[str, float] = {}
            for index in range(len(labels)):
                to_add[labels[index]] = guess_result[index]
            result.append(to_add)
        return result


def get_max_key(result: Dict[K, float]) -> K:
    """
    Returns the key for which the result is at maximum

    :param result: The result given by the classifier
    :return: The key for which the value is at maximum, None if the dictionnary is empty
    """
    max_value: float = -1
    max_key: K = None
    for key in result.keys():
        if result[key] >= max_value:
            max_key = key
            max_value = result[key]
    return max_key


def get_max_key_multiple(result: List[Dict[K, float]]) -> Tuple[int, K]:
    """
    Return the most sure result in a list of classifier result

    :param result: A list of results given by the classifier
    :return: The idx and key that gives the best result
    """
    if len(result) == 0:
        return -1, None
    max_idx: int = 0
    max_idx_key: K = get_max_key(result[0])
    for i in range(1, len(result)):
        max_i_key = get_max_key(result[i])
        if max_i_key is not None and result[i][max_i_key] > result[max_idx][max_idx_key]:
            max_idx = i
            max_idx_key = max_i_key
    return max_idx, max_idx_key


def get_all_unsure_results(results: List[Dict[K, float]], minimum_value: float, minimum_delta: float = 0) -> List[int]:
    """
    Returns all indexes for whose the result associated in a list is unsure.

    :param results: A list of result that were returned by a classifier
    :param minimum_value: The minimum certitude that a result must contain to be certified
    :param minimum_delta: The minimum delta range that a result must be from the others to be certified
    :return: A list of index
    """
    idx_list = []
    for i in range(0, len(results)):
        max_key = get_max_key(results[i])
        if results[i][max_key] < minimum_value:
            idx_list.append(i)
        else:
            delta = 1
            for key in results[i].keys():
                if key != max_key and results[i][max_key] - results[i][key] < delta:
                    delta = results[i][max_key] - results[i][key]
            if delta < minimum_delta:
                idx_list.append(i)
    return idx_list
