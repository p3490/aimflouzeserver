from classifier.classifier import Classifier
from typing import List, Dict


class PieceSizeClassifier(Classifier[int, float]):
    """
    This classifier identifies coin based on their diameter size.
    The keys are the different coins values in cents.
    The excepted samples are diameters of the coins

    :param reference_value: The value of the coin reference, in cents
    :param reference_radius: The height and width of the coin reference
    """

    coin_diameters: Dict[int, float] = {
        5: 21.25,
        10: 19.75,
        20: 22.25,
        50: 24.25,
        100: 23.25,
        200: 25.75
    }

    def __init__(self, reference_value: int, reference_radius: float):
        self.reference_value = reference_value
        self.reference_radius = reference_radius

    # Overrides the abstract method from Classifier
    def guess(self, sample: float) -> List[float]:
        coin_image_ratio = sample / self.reference_radius
        result = []
        for coin_value in self.get_label():
            calculated_ratio = self.coin_diameters[coin_value] / self.coin_diameters[self.reference_value]
            result.append(min(coin_image_ratio, calculated_ratio) / max(coin_image_ratio, calculated_ratio))
        return result

    # Overrides the abstract method from Classifier
    def get_label(self) -> List[int]:
        return [5, 10, 20, 50, 100, 200]
