from classifier.classifier import Classifier
from typing import List
from classifier.coins.piece_data import PieceImageData
from image_utils.picture_utils import show_image, get_image_size, get_all_pixels_in_circle, reduce_to_n_colors_set, \
    get_all_pixels_in_donut
from image_utils.color_util import get_distance_between_colors, get_saturation_value
import cv2
import numpy as np


class DoubleCircleClassifier(Classifier[int, PieceImageData]):
    """
    A classifier that bases his choice on the two circles that can be seen in a coin of 1 or 2 euros.

    :param min_distance_color: The minimum distance between the color of two circles
    :param min_saturation_distance: The minimum distance between the
    """

    # The ration between the radius of the little circle and of the big circle
    radius_ratios = 430.0 / 600.0

    def __init__(self, min_distance_color: float = 15, min_saturation_distance: float = 10):
        self.min_distance_color = min_distance_color
        self.min_saturation_distance = min_saturation_distance

    # Overrides the abstract method from Classifier
    def guess(self, sample: PieceImageData) -> List[float]:
        # Get the piece and format it to a square
        piece_image = sample.get_cropped_image(with_padding=False)
        if min(get_image_size(piece_image)) <= 0:
            return [0, 0, 0, 0, 0, 0, 0]
        image_size = max(get_image_size(piece_image))
        piece_image = cv2.resize(piece_image, (image_size, image_size))

        # Calculate the radius of each circle
        max_radius = int(image_size / 2)
        min_radius = int(max_radius * self.radius_ratios)
        center = int(image_size / 2)

        # Determines the main color in each circle, and the distance between these two colors
        little_circle_pixels = np.asarray([get_all_pixels_in_circle(piece_image, center, center, min_radius)])
        min_circle_color = reduce_to_n_colors_set(little_circle_pixels, number_of_colors=1).pop()
        big_circle_pixels = np.asarray([get_all_pixels_in_donut(piece_image, center, center, min_radius, max_radius)])
        max_radius_color = reduce_to_n_colors_set(big_circle_pixels, number_of_colors=1).pop()
        color_distance = get_distance_between_colors(min_circle_color, max_radius_color)

        if color_distance > self.min_distance_color:
            # CASE 1 : There is a big bifference between each circle
            euro_chance = min((color_distance - self.min_distance_color) / self.min_distance_color, 0.5) + 0.5

            little_circle_saturation = get_saturation_value(min_circle_color)
            print("Little circle saturation : " + str(little_circle_saturation))
            max_radius_saturation = get_saturation_value(max_radius_color)
            print("Big circle saturation : " + str(max_radius_saturation))

            saturation_distance = max_radius_saturation - little_circle_saturation
            if saturation_distance > 0:
                # CASE 1.1 : The little circle is less saturated
                one_euro_chance = min(saturation_distance, self.min_saturation_distance / 2) / (
                        self.min_saturation_distance / 2)
            else:
                # CASE 1.1 : The little circle is more satureted
                one_euro_chance = 1 - (min(abs(saturation_distance), self.min_saturation_distance / 2) / (
                        self.min_saturation_distance / 2))
            return [
                1 - euro_chance,
                1 - euro_chance,
                1 - euro_chance,
                1 - euro_chance,
                0.5 + (one_euro_chance / 2),
                1 - (one_euro_chance / 2)
            ]
        else:
            # CASE 2 : There is not much difference between each circle
            euro_chance = (color_distance / self.min_distance_color) / 2
            return [
                1 - euro_chance,
                1 - euro_chance,
                1 - euro_chance,
                1 - euro_chance,
                euro_chance,
                euro_chance
            ]

    # Overrides the abstract method from Classifier
    def get_label(self) -> List[int]:
        return [5, 10, 20, 50, 100, 200]
