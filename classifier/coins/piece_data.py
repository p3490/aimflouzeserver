from typing import Any, List, Tuple
from image_utils.picture_utils import crop_rectangle_from_image


class PieceImageData(object):
    """
    A class representing data for piece analysis.

    :param full_image: The image of the piece
    :param padding: The padding added to each size of the image, in pixels
    :param position: The position of the piece in the image
    """

    def __init__(self, full_image: Any, padding: int, position: Tuple[int, int, int, int, int]):
        self.full_image = full_image
        self.padding = padding
        self.position = position

    def get_cropped_image(self, with_padding: bool = True) -> Any:
        if not with_padding:
            return crop_rectangle_from_image(self.full_image,
                                             self.position[0] + self.padding,
                                             self.position[1] + self.padding,
                                             self.position[2] - self.padding * 2,
                                             self.position[3] - self.padding * 2,
                                             )
        else:
            return crop_rectangle_from_image(self.full_image,
                                             self.position[0],
                                             self.position[1],
                                             self.position[2],
                                             self.position[3]
                                             )


class MultiplePieceImageData(object):
    """
    A class that contains data in multiples pieces contained in multiple photos

    :param pieces: A rectangular matrice of PieceImageData. The first dimensions is sorted by pieces. The second
                   dimension is sorted by images.
    """

    def __init__(self, pieces: List[List[PieceImageData]]):
        self.pieces = pieces

    def get_piece_number(self) -> int:
        return len(self.pieces)

    def get_image_number(self) -> int:
        return len(self.pieces[0])

    def get_all_piece_data_of_piece(self, index: int) -> List[PieceImageData]:
        return self.pieces[index]

    def get_all_piece_data_of_image(self, index: int) -> List[PieceImageData]:
        return [self.pieces[i][index] for i in range(self.get_piece_number())]

    def get_piece(self, piece_idx: int, image_idx: int) -> PieceImageData:
        return self.pieces[piece_idx][image_idx]
