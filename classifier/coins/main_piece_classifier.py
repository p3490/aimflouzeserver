from classifier.dict_based_classifier import MultipleDictBasedClassifier
from typing import List, Tuple, Dict
from classifier.model_classifier import ModelClassifier
from classifier.coins.piece_data import PieceImageData
from classifier.coins.piece_size_detection import PieceSizeClassifier
from image_utils.picture_utils import get_image_size, crop_image
from classifier.coins.double_circle_classifier import DoubleCircleClassifier


def _get_best_tail(result: Dict[str, float]) -> Tuple[int, float]:
    """
    Get the best tail value, along with the peercentage

    :param result: The model result
    :return: The best tail value, and his certitude peercentage
    """
    max_percentage: float = 0
    max_idx_key: str = ""
    for key in result.keys():
        if key.startswith("P") and result[key] >= max_percentage:
            max_idx_key = key
            max_percentage = result[key]
    return int(max_idx_key[1:]), max_percentage


def _get_best_tail_multiple(results: List[Dict[str, float]]) -> Tuple[int, int, float]:
    """
    Get the best tail value, along with the peercentage, in a list of piece guesses

    :param results: The model result list
    :return: The best taill index, value, and his certitude peercentage
    """
    max_idx = 0
    max_value, max_percentage = _get_best_tail(results[0])
    for i in range(1, len(results)):
        i_value, i_percentage = _get_best_tail(results[i])
        if max_percentage < i_percentage:
            max_percentage = i_percentage
            max_value = i_value
            max_idx = i
    return max_idx, max_value, max_percentage


def _merge_head_tail(result: Dict[str, float]) -> Dict[int, float]:
    """
    Keeps only the tails value for the moment

    :param result: The model guesses
    :return: The merged results
    """
    merge_result: Dict[int, float] = dict()
    for key in result.keys():
        value = int(key[1:])
        if key.startswith("P"):
            merge_result[value] = result[key]
    return merge_result


def _merge_results(model_result: Dict[int, float],
                   size_result: Dict[int, float],
                   double_circle_result: Dict[int, float]) -> Dict[int, float]:
    """
    Merges the result of three differents models

    :param model_result: The model guesses
    :param size_result: The size guesses
    :param double_circle_result: The double circle guesses
    :return: The final result
    """
    result = {}
    for key in model_result.keys():
        result[key] = model_result[key] * double_circle_result[key] * size_result[key]
    return result


class MainPieceClassifier(MultipleDictBasedClassifier[int, PieceImageData]):
    """
    This is the main classifier of pieces.
    It (will) uses all sort of classifiers to determine if samples are samples of pieces or not.
    His keys corresponds to the value of europeans coins, in cents.
    His excepted samples are PiecesData.

    :param model_path: The path to the model
    :param labels_path: The path to the file of labels
    :param image_dimension: The dimension excepted for the samples
    """

    def __init__(self, model_path: str, labels_path: str, image_dimension: Tuple[int, int, int]):
        self.head_tail_classifier = ModelClassifier(model_path, labels_path, image_dimension)
        self.double_size_classifier = DoubleCircleClassifier()

    # Overrides the method of MultipleDictBasedClassifier
    def get_label(self) -> List[int]:
        return [5, 10, 20, 50, 100, 200]

    # Overrides the method of MultipleDictBasedClassifier
    def guess_multiples_with_labels(self, samples: List[PieceImageData]) -> List[Dict[int, float]]:
        # First analyse with model
        images = list(map(lambda e: e.get_cropped_image(with_padding=False), samples))
        guesses_model = self.head_tail_classifier.guess_multiples_with_labels(images)
        print("Modele guesser : " + str(guesses_model))

        # Get the best tail piece and convert the head/tail differenciated pieces to simple value result
        best_piece_idx, best_piece_value, _ = _get_best_tail_multiple(guesses_model)
        guesses_model = list(map(lambda e: _merge_head_tail(e), guesses_model))

        # Apply the additional guesses
        size_guesser = PieceSizeClassifier(best_piece_value,
                                           max(get_image_size(samples[best_piece_idx].get_cropped_image())))
        guesses_size = size_guesser.guess_multiples_with_labels(
            list(map(lambda piece: max(piece.position[2], piece.position[3]), samples))
        )
        print("Size guesser : " + str(guesses_size))
        guesses_double_circle = self.double_size_classifier.guess_multiples_with_labels(samples)
        print("Double circle guesser : " + str(guesses_double_circle))

        # Calculate a merge result and return it
        result: List[Dict[int, float]] = []
        for i in range(len(samples)):
            result.append(_merge_results(guesses_model[i], guesses_size[i], guesses_double_circle[i]))

        print("Resultat final : " + str(result))
        return result
