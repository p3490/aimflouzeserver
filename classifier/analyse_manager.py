import uuid
from threading import Thread
from typing import Dict, Tuple, NoReturn, Any, Iterable, List
from database_utils.database_utils import DatabaseUtils
import cv2
import os
from image_utils.geometry_detection_utils import detect_independent_box
from .classifier import Classifier, get_max_key
from classifier.coins.piece_data import PieceImageData, MultiplePieceImageData
from classifier.coins.main_piece_classifier import MainPieceClassifier
import numpy as np
from image_utils.picture_utils import get_image_size
import logging


def get_all_piece_one_image(image: Any) -> List[PieceImageData]:
    """
    Returns all pieces that are found in a image

    :param image: The source image
    :return: A list of PieceImageData found in this picture
    """
    box_size = int(max(get_image_size(image)) * (61 / 3264))
    if box_size % 2 == 0:
        box_size += 1
    print("BOX SIZE : " + str(box_size))
    data = detect_independent_box(image, box_size=box_size)
    return list(map(lambda position: PieceImageData(image, box_size, position), data))


def get_all_pieces_position(images: List[Any]) -> MultiplePieceImageData:
    """
    Returns all pieces found in a list of similar pieces.
    All pieces that are returned are found approximately at the same place for all images

    :param images: The source images
    :return: A MultiPieceImageData, that describes all pieces found in the images
    """

    # Set the matrice of PieceImageData with the pieces found in the first image
    piece_image_groups: List[List[PieceImageData]] = []
    for piece_image_data in get_all_piece_one_image(images[0]):
        piece_image_groups.append([piece_image_data])

    if 1 < len(images):
        # For each other images
        for image_idx in range(1, len(images)):
            piece_image_datas = get_all_piece_one_image(images[image_idx])
            piece_image_datas.reverse()
            # For each piece found in the image
            for piece_image_data in piece_image_datas:
                i = 0
                # For each piece group in the matrice
                while i < len(piece_image_groups):
                    group_position = piece_image_groups[i][0].position
                    center = (piece_image_data.position[cv2.CC_STAT_LEFT] + (piece_image_data.position[cv2.CC_STAT_WIDTH] / 2),
                              piece_image_data.position[cv2.CC_STAT_TOP] + (piece_image_data.position[cv2.CC_STAT_HEIGHT] / 2))
                    # If the center of the forms correspond to the main piece group position
                    if group_position[cv2.CC_STAT_LEFT] < center[0] < \
                            group_position[cv2.CC_STAT_LEFT] + group_position[cv2.CC_STAT_WIDTH] and \
                            group_position[cv2.CC_STAT_TOP] < center[1] < \
                            group_position[cv2.CC_STAT_TOP] + group_position[cv2.CC_STAT_HEIGHT]:
                        # Add the piece in the piece group
                        piece_image_groups[i].append(
                            PieceImageData(
                                piece_image_data.full_image,
                                piece_image_data.padding,
                                (
                                    int(center[0] - group_position[cv2.CC_STAT_WIDTH] / 2),
                                    int(center[1] - group_position[cv2.CC_STAT_HEIGHT] / 2),
                                    group_position[cv2.CC_STAT_WIDTH],
                                    group_position[cv2.CC_STAT_HEIGHT],
                                    1
                                )
                            )
                        )
                        i = len(piece_image_groups)
                    else:
                        # Else, pass to the next form group
                        i += 1

    i = 0
    # Remove all group that has more forms or less forms that the number of images in the input
    while i < len(piece_image_groups):
        if len(piece_image_groups[i]) < len(images):
            del piece_image_groups[i]
        elif len(piece_image_groups[i]) > len(images):
            piece_image_groups[i] = [piece_image_groups[i][j] for j in range(0, len(images))]
            i += 1
        else:
            i += 1
    return MultiplePieceImageData(piece_image_groups)


class AnalyseManager(object):
    """
    An analyse manager. It manages all threads and analyses
    """
    def __init__(self, database_util: DatabaseUtils, image_folder_path: str,
                 model_path: str, labels_path: str, image_dimension: Tuple[int, int, int]):
        self.threadMap: Dict[str, Thread] = {}
        self.database_util: DatabaseUtils = database_util
        self.image_folder_path: str = image_folder_path
        self.classifier: Classifier[int, PieceImageData] = MainPieceClassifier(model_path, labels_path, image_dimension)

    def launch_analyse_thread(self, analyse_uuid: str) -> bool:
        """
        Launch an analyse thread. It also delete all finished threads

        :param analyse_uuid: Analyse to do
        :return: True if the analyse started succesfuly, false otherwise
        """
        keys = set(self.threadMap.keys())
        for key in keys:
            if not self.threadMap[key].is_alive():
                del self.threadMap[key]
        self.threadMap[analyse_uuid] = Thread(target=self._launch_analyse, args=[analyse_uuid])
        try:
            self.threadMap[analyse_uuid].start()
        except RuntimeError:
            return False
        return True

    def _save_and_add_image(self, image: Any, analyse_uuid: str) -> NoReturn:
        """
        Saves and add a image of development to the database.

        :param image: The image to save
        :param analyse_uuid: The analyse uuid
        """
        id: str = str(uuid.uuid4())
        path = os.path.join(self.image_folder_path, id + ".jpg")
        cv2.imwrite(path, image)
        self.database_util.add_image(id, False, True, analyse_uuid)

    def _add_dev_images(self, image_src: Any, analyse_uuid: str) -> NoReturn:
        """
        Add a series of "debug images" to the database.

        :param image_src: The source image
        :param analyse_uuid: The analyse uuid
        """
        # This is basically a copy/paste of functions in imageUtils, for debugging
        image = cv2.GaussianBlur(image_src, (15, 15), 0)
        rgb_planes = cv2.split(image)
        result_norm_planes = []
        for plane in rgb_planes:
            dilated_img = cv2.dilate(plane, np.ones((7, 7), np.uint8))
            bg_img = cv2.medianBlur(dilated_img, 21)
            diff_img = 255 - cv2.absdiff(plane, bg_img)
            norm_img = cv2.normalize(diff_img, None, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8UC1)
            result_norm_planes.append(norm_img)
        image = cv2.merge(result_norm_planes)
        self._save_and_add_image(image, analyse_uuid)

        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        _, image = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        image = cv2.adaptiveThreshold(image, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 61, 0)
        self._save_and_add_image(image, analyse_uuid)

    def _launch_analyse(self, analyse_uuid: str) -> NoReturn:
        """
        Launch an analyse

        :param analyse_uuid: The uuid of the analyse
        """
        try:
            self._launch_analyse_errorfull(analyse_uuid)
        except Exception:
            logging.exception("Analyse exception: ")
            self.database_util.set_analyse_error(analyse_uuid, True)
            self.database_util.set_analyse_finished(analyse_uuid, True)

    def _launch_analyse_errorfull(self, analyse_uuid: str) -> NoReturn:
        """
        Launch an analyse. It can return various errors.

        :param analyse_uuid: The uuid of the analyse
        """
        # Image loading
        analyse = self.database_util.get_analyse_of_uuid(analyse_uuid)
        images = sorted(analyse.images, key=lambda e: e.with_flash)
        images = list(map(lambda e: cv2.imread(os.path.join(self.image_folder_path, e.uuid + ".jpg")), images))

        self._add_dev_images(images[0], analyse_uuid)
        if len(images) > 1:
            self._add_dev_images(images[1], analyse_uuid)

        pieces_data = get_all_pieces_position(images)

        print("Photo 0")
        print("")
        final_guesses = self.classifier.guess_multiples_with_labels(pieces_data.get_all_piece_data_of_image(0))
        if 1 < pieces_data.get_image_number():
            for image_idx in range(1, pieces_data.get_image_number()):
                print("Photo " + str(image_idx))
                print("")
                new_guesses = self.classifier.guess_multiples_with_labels(pieces_data.get_all_piece_data_of_image(image_idx))
                for dic_idx in range(len(new_guesses)):
                    for key in final_guesses[dic_idx].keys():
                        final_guesses[dic_idx][key] *= new_guesses[dic_idx][key]

        print("RESULTAT FINAL : " + str(final_guesses))

        # Entering of the result made by the size_classifier
        for piece_idx in range(0, len(final_guesses)):
            result = final_guesses[piece_idx]
            max_key = get_max_key(result)
            coordinates = tuple(int(x) for x in pieces_data.get_piece(piece_idx, 0).position[0:4])
            self.database_util.add_piece(max_key, coordinates[0], coordinates[1], coordinates[2],
                                         coordinates[3], result[max_key], analyse_uuid)
        self.database_util.set_analyse_finished(analyse_uuid, True)
