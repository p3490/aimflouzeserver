from firebase_admin import credentials, firestore
from firebase_admin.auth import Client, InvalidIdTokenError, ExpiredIdTokenError, RevokedIdTokenError, UserDisabledError, CertificateFetchError
import firebase_admin
from typing import NoReturn
from cryptography.hazmat.primitives.asymmetric import rsa, padding
from cryptography.hazmat.primitives import serialization, hashes


class FireBaseUtils(object):
    """
    Main class used to contact firebase
    """

    def __init__(self, key_path: str, private_key: bytes, public_key: bytes):
        self.cred = credentials.Certificate(key_path)
        try:
            self.app = firebase_admin.initialize_app(self.cred, {'databaseURL': 'https://databaseName.firebaseio.com'})
        except ValueError as e:
            self.app = firebase_admin.get_app()
        self.db = firestore.client(self.app)
        self.client = Client(self.app)
        self.private_key = serialization.load_pem_private_key(
            private_key,
            password=None
        )
        self.public_key = serialization.load_pem_public_key(
            public_key
        )
        self.padding = padding.OAEP(
            mgf=padding.MGF1(algorithm=hashes.SHA256()),
            algorithm=hashes.SHA256(),
            label=None
        )

    def check_token(self, token: str) -> bool:
        """
        This methods check if a token is valid or not

        :param token: The token to check
        :return: True if the token is valid, False if it's not, IOError for any other error type
        """
        try:
            self.client.verify_id_token(token, True)
            return True
        except ValueError:
            return False

    def get_token_data(self, token: str) -> dict:
        """
        This methods get the data of a given token

        :param token: The token to check
        :return: The data of the token in a dict, ValueError if the token is invalid, IOError for other issues
        """
        try:
            return self.client.verify_id_token(token, True)
        except (InvalidIdTokenError, ExpiredIdTokenError, RevokedIdTokenError, UserDisabledError) as e:
            raise ValueError()
        except CertificateFetchError:
            raise IOError()

    def get_uid(self, token: str) -> str:
        """
        Return the UID of the user token
        :param token: The token to check
        :return: The UID associated to this token, ValueError if the token is invalid, IOError for other issues
        """
        return self.get_token_data(token)['uid']

    def get_account_data(self, uid: str) -> str:
        """
        Return the rights associated to the user token
        :param uid: The UID of the user to add
        :return: The user data, in form of a dict
        """
        document = self.db.collection('accounts').document(uid).get()
        if document.exists:
            data = document.to_dict()
            if 'first_name' in data and data['first_name'] is not None:
                data['first_name'] = self.private_key.decrypt(data['first_name'], self.padding).decode("utf-8")
            if 'last_name' in data and data['last_name'] is not None:
                data['last_name'] = self.private_key.decrypt(data['last_name'], self.padding).decode("utf-8")
            return data
        else:
            self.add_empty_account_document(uid)
            return self.get_account_data(uid)

    def add_empty_account_document(self, uid: str) -> NoReturn:
        """
        Creates a empty account document, if none were created before.
        The user will be given the "user" role
        :param uid: The UID of the user to add
        """
        self.db.collection('accounts').document(uid).set({
            'rights': 'user',
            'first_name': None,
            'last_name': None
        })

    def set_account_document(self, uid: str, first_name: str, last_name: str, right: str) -> NoReturn:
        """
        Set an account document to the firebase datastore. The first and last name will be stored crypted.

        :param uid: The UID of the user
        :param first_name: The first name of the user
        :param last_name: The last name of the user
        :param right: The user rights
        """
        if first_name is not None:
            first_name = self.public_key.encrypt(first_name.encode("utf-8"), self.padding)
        if last_name is not None:
            last_name = self.public_key.encrypt(last_name.encode("utf-8"), self.padding)
        self.db.collection('accounts').document(uid).set({
            'rights': right,
            'first_name': first_name,
            'last_name': last_name
        })

    def delete_account_document(self, uid: str) -> NoReturn:
        """
        Delete an user from the firebase datastore

        :param uid: The UID of the user
        """
        self.db.collection('accounts').document(uid).delete()
