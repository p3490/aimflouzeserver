import datetime
import sqlite3
from typing import NoReturn, Set, Optional
from models.image_model import ImageModel
from models.piece_model import PieceModel
from models.analyse_model import AnalyseModel
import os


class DatabaseUtils(object):
    def __init__(self, database_path: str):
        self.database_path = database_path
        os.makedirs(os.path.dirname(self.database_path), exist_ok=True)
        with open(self.database_path, "a+"):
            pass
        self.initialise_tables()

    def initialise_tables(self) -> NoReturn:
        """
        Initialise all the necessary tables for the database.
        Does basically nothing if the tables have allready been created.
        """
        conn = sqlite3.connect(self.database_path)
        conn.execute("CREATE TABLE IF NOT EXISTS ANALYSES"
                     "(uuid CHAR(36),"
                     "user_id VARCHAR(128) NOT NULL,"
                     "date_of_post DATETIME NOT NULL,"
                     "finished BOOLEAN NOT NULL DEFAULT FALSE,"
                     "error BOOLEAN NOT NULL DEFAULT FALSE,"
                     "CONSTRAINT pk_analyses PRIMARY KEY ('uuid'))")
        conn.execute("CREATE TABLE IF NOT EXISTS IMAGES"
                     "(uuid CHAR(36),"
                     "with_flash BOOLEAN NOT NULL,"
                     "is_dev BOOLEAN NOT NULL,"
                     "analyse_uuid CHAR(36),"
                     "CONSTRAINT pk_images PRIMARY KEY ('uuid'),"
                     "CONSTRAINT fk_images_analyse FOREIGN KEY ('analyse_uuid') REFERENCES ANALYSES('uuid')"
                     "ON UPDATE CASCADE ON DELETE SET NULL)")
        conn.execute("CREATE TABLE IF NOT EXISTS PIECES"
                     "(id INTEGER PRIMARY KEY AUTOINCREMENT,"
                     "value_cents SMALLINT,"
                     "position_x UNSIGNED TINYINT,"
                     "position_y UNSIGNED INT,"
                     "width UNSIGNED INT,"
                     "height UNSIGNED INT,"
                     "accuracy UNSIGNED FLOAT,"
                     "analyse_uuid CHAR(36),"
                     "CONSTRAINT fk_pieces_analyse FOREIGN KEY ('analyse_uuid') REFERENCES ANALYSES('uuid')"
                     "ON UPDATE CASCADE ON DELETE CASCADE)")

    def get_connection(self) -> sqlite3.Connection:
        """
        Return a connection with foreign_keys enabled

        :return: A connection to the database
        """
        conn = sqlite3.connect(self.database_path, detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES)
        conn.execute("PRAGMA foreign_keys = 1")
        return conn

    def add_analyse(self, uuid: str, user_id: str, date_of_post: datetime.datetime) -> NoReturn:
        """
        Add an analyse to the database

        :param uuid: The uuid given to this analyse
        :param user_id: The user id
        :param date_of_post: The date at whitch the analyse started
        """
        conn = self.get_connection()
        conn.execute("INSERT INTO ANALYSES(uuid, user_id, date_of_post) VALUES(?, ?, ?)", [
            uuid,
            user_id,
            date_of_post
        ])
        conn.commit()
        conn.close()

    def add_image(self, uuid: str, with_flash: bool, is_dev: bool, analyse_uuid: str = None):
        """
        Add an image to the database

        :param uuid: The uuid given to the image
        :param with_flash: Does the image was taken with a flash
        :param analyse_uuid: The associated image uuid
        :param is_dev: Was the image created during dev process?
        """
        conn = self.get_connection()
        conn.execute("INSERT INTO IMAGES(uuid, with_flash, analyse_uuid, is_dev) VALUES(?, ?, ?, ?)", [
            uuid,
            with_flash,
            analyse_uuid,
            is_dev
        ])
        conn.commit()
        conn.close()

    def add_piece(self, value_cents: int, position_x: int, position_y: int,
                  width: int, height: int, accuracy: float, analyse_uuid: str):
        """
        Add a piece to the database

        :param value_cents: The value of the piece, in cents
        :param position_x: The position of the piece in the picture in the x axis
        :param position_y: The position of the piece in the picture in the y axis
        :param width: The width of the piece in the picture
        :param height: The height of the piece in the picture
        :param accuracy: The accuracy associated to this piece detection
        :param analyse_uuid: The associated analyse uuid
        """
        conn = self.get_connection()
        conn.execute("INSERT INTO PIECES(value_cents, position_x, position_y, width, height, accuracy, analyse_uuid)"
                     "VALUES(?, ?, ?, ?, ?, ?, ?)", [
                        value_cents, position_x, position_y,
                        width, height, accuracy, analyse_uuid
                     ])
        conn.commit()
        conn.close()

    def get_all_images_of_analyse(self, analyse_uuid: str) -> Set[ImageModel]:
        """
        Get all images data of an analyse as a set of ImageModels.

        :param analyse_uuid: The analyse uuid
        :return: A set of ImageModel
        """
        conn = self.get_connection()
        cursor = conn.cursor()
        cursor.execute("SELECT uuid, with_flash, is_dev FROM IMAGES WHERE analyse_uuid = ?",
                       [analyse_uuid])
        result = set(map(lambda e: ImageModel(e[0], bool(e[1]), bool(e[2])), cursor.fetchall()))
        cursor.close()
        return result

    def get_all_pieces_of_analyse(self, analyse_uuid: str) -> Set[PieceModel]:
        """
        Get all pieces data of an analyse as a set of PieceModels

        :param analyse_uuid: The analyse uuid
        :return: A set of PieceModel
        """
        conn = self.get_connection()
        cursor = conn.cursor()
        cursor.execute("SELECT id, value_cents, position_x, position_y, width, height, accuracy FROM PIECES WHERE analyse_uuid = ?", [analyse_uuid])
        result = set(map(lambda e: PieceModel(e[0], e[1], e[2], e[3], e[4], e[5], e[6]), cursor.fetchall()))
        cursor.close()
        return result

    def get_all_analyses_of_user(self, user_id: str) -> Set[AnalyseModel]:
        """
        Get all analyses data of a user as a set of AnalyseModel

        :param user_id: The user uuid
        :return: A set of AnalyseModel
        """
        conn = self.get_connection()
        cursor = conn.cursor()
        print(user_id)
        cursor.execute("SELECT uuid, date_of_post, finished, error FROM ANALYSES WHERE user_id = ?",
                       [user_id])
        result = set(map(lambda e: AnalyseModel(
            e[0],
            user_id,
            e[1],
            bool(e[2]),
            bool(e[3]),
            self.get_all_images_of_analyse(e[0]),
            self.get_all_pieces_of_analyse(e[0])
        ), cursor.fetchall()))
        cursor.close()
        return result

    def get_analyse_of_uuid(self, uuid: str) -> Optional[AnalyseModel]:
        """
        Return the analyse data of a given uuid as a AnalyseModel

        :param uuid: The uuid of the analyse
        :return: An AnalyseModel if one of this given uuid exists, None otherwise
        """
        conn = self.get_connection()
        cursor = conn.cursor()
        cursor.execute("SELECT user_id, date_of_post, finished, error FROM ANALYSES WHERE uuid = ?",
                       [uuid])
        entry = cursor.fetchone()
        cursor.close()
        if entry is None:
            return None
        else:
            return AnalyseModel(
                uuid,
                entry[0],
                entry[1],
                bool(entry[2]),
                bool(entry[3]),
                self.get_all_images_of_analyse(uuid),
                self.get_all_pieces_of_analyse(uuid)
            )

    def get_user_of_image(self, uid: str) -> Optional[str]:
        """
        Return the user id whose image belongs to

        :param uid: The uuid of the image
        :return: The user id whose image belongs to if it exists, None otherwise
        """
        conn = self.get_connection()
        cursor = conn.cursor()
        cursor.execute("SELECT user_id FROM ANALYSES WHERE uuid IN (SELECT analyse_uuid FROM IMAGES WHERE uuid = ?)",
                       [uid])
        result = cursor.fetchone()
        cursor.close()
        if result is None:
            return None
        else:
            return result[0]

    def remove_analyse(self, uid: str) -> NoReturn:
        """
        Remove an analyse from the database.
        Does not delete the associated images!

        :param uid: THe uuid of the analyse
        """
        conn = self.get_connection()
        conn.execute("DELETE FROM ANALYSES WHERE uuid = ?", [uid])
        conn.commit()
        conn.close()

    def remove_image(self, uid: str) -> NoReturn:
        """
        Remove an image from the database.

        :param uid: The uuid of the image
        """
        conn = self.get_connection()
        conn.execute("DELETE FROM IMAGES WHERE uuid = ?", [uid])
        conn.commit()
        conn.close()

    def set_analyse_finished(self, uid: str, finished: bool) -> NoReturn:
        """
        Changes the finished attribute of an analyse

        :param uid: The uuid of the analyse
        :param finished: Is the analyse finished?
        """
        conn = self.get_connection()
        conn.execute("UPDATE ANALYSES SET finished = ? WHERE uuid = ?", [finished, uid])
        conn.commit()
        conn.close()

    def get_all_images_of_piece(self, pid: str) -> Set[ImageModel]:
        """
        Returns all images that are associated to a piece

        :param pid: The piece id
        :return: A set of images associated to the piece
        """
        conn = self.get_connection()
        cursor = conn.cursor()
        cursor.execute("SELECT uuid, with_flash, is_dev FROM IMAGES "
                       "WHERE analyse_uuid IN (SELECT analyse_uuid FROM PIECES WHERE id = ?)",
                       [pid])
        result = set(map(lambda e: ImageModel(e[0], bool(e[1]), bool(e[2])), cursor.fetchall()))
        cursor.close()
        return result

    def get_user_of_piece(self, pid: str) -> Optional[str]:
        """
        Returns the user associated to a piece

        :param pid: The piece id
        :return: The user associated to the piece, or None if not found
        """
        conn = self.get_connection()
        cursor = conn.cursor()
        cursor.execute("SELECT user_id FROM ANALYSES "
                       "WHERE uuid IN (SELECT analyse_uuid FROM PIECES WHERE id = ?)",
                       [pid])
        result = cursor.fetchone()
        if result is None:
            return None
        else:
            return result[0]

    def get_piece(self, pid: str) -> Optional[PieceModel]:
        """
        Returns the piece of id given.

        :param pid: The piece id
        :return: The piece, or None if not found
        """
        conn = self.get_connection()
        cursor = conn.cursor()
        cursor.execute("SELECT id, value_cents, position_x, position_y, width, height, accuracy "
                       "FROM PIECES WHERE id = ?", [pid])
        result = cursor.fetchone()
        if result is None:
            return None
        else:
            return PieceModel(
                result[0],
                result[1],
                result[2],
                result[3],
                result[4],
                result[5],
                result[6]
            )

    def set_analyse_error(self, analyse_uuid: str, error: bool = True) -> NoReturn:
        """
        Set the error attribute of an analysis of given uuid

        :param analyse_uuid: The analysis uuid
        :param error: The error value
        """
        conn = self.get_connection()
        conn.execute("UPDATE ANALYSES SET error = ? WHERE uuid = ?", [error, analyse_uuid])
        conn.commit()
        conn.close()
