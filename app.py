import base64
import datetime
import json

from flask import Flask, Response, send_file, abort
from flask_classful import FlaskView, route, request
import os
from firebase_utils.firebase_utils import FireBaseUtils
from database_utils.database_utils import DatabaseUtils
from dateutil import parser as dateparser
from typing import List
import uuid
from classifier.analyse_manager import AnalyseManager
import cv2
from image_utils.picture_utils import crop_rectangle_from_image

app = Flask(__name__)
THIS_DIR = os.path.dirname(os.path.abspath(__file__))

firebase_key = os.path.join(THIS_DIR, "rsc", "firebaseKey.json")
with open(os.path.join(THIS_DIR, "rsc", "public.pem"), 'rb') as public_key_file:
    public_key = public_key_file.read()
with open(os.path.join(THIS_DIR, "rsc", "key.pem"), 'rb') as private_key_file:
    private_key = private_key_file.read()


class MainView(FlaskView):

    def __init__(self):
        self.working_directory = os.path.join(THIS_DIR, "tmp")
        self.firebase_utils = FireBaseUtils(firebase_key, private_key, public_key)
        self.database_utils = DatabaseUtils(os.path.join(self.working_directory, "database.db"))
        self.analyse_manager = AnalyseManager(self.database_utils, os.path.join(self.working_directory, "images"),
                                              os.path.join(THIS_DIR, "output", "model_head_tail.h5"),
                                              os.path.join(THIS_DIR, "output", "lab_head_tail.pickle"),
                                              (150, 150, 3))

    def get_uuid(self) -> str:
        if "x-access-token" not in request.headers:
            print("PAS DE TOKEN")
            abort(Response("NOK : Aucun token", status=401))
        token = request.headers["x-access-token"]
        try:
            try:
                uid = self.firebase_utils.get_uid(token)
                return uid
            except ValueError:
                abort(Response("NOK : Token invalide", status=401))
        except IOError:
            abort(Response("NOK : Firebase ne marche pas", status=500))
        return ""

    @route("/analyse", methods=["POST"])
    def analyse(self) -> Response:
        if "x-access-token" not in request.headers:
            return Response("NOK : Aucun token", status=401)
        if not request.headers['Content-Type'].startswith("application/json"):
            return Response("NOK : Le contenu n'est pas du JSON", status=401)
        if 'date' not in request.json \
                or 'images' not in request.json:
            return Response("NOK : Il manque des champs dans la requête", status=400)

        # Append the analyse in the database
        token = request.headers["x-access-token"]
        try:
            auth_uid = self.firebase_utils.get_uid(token)
        except ValueError:
            return Response("NOK : Token invalide", status=401)
        date: datetime.datetime = dateparser.parse(request.json['date'])
        analyse_uuid = str(uuid.uuid4())
        self.database_utils.add_analyse(analyse_uuid, auth_uid, date)

        # Add each image in the database
        images_data: List = []
        for i in range(len(request.json['images'])):
            image = request.json['images'][i]
            byte_tab: bytes = base64.standard_b64decode(image['imageFile'])
            file_uuid = str(uuid.uuid4())
            file_path = os.path.join(self.working_directory, "images", file_uuid + ".jpg")
            os.makedirs(os.path.join(self.working_directory, "images"), exist_ok=True)
            with open(file_path, "xb") as file_writer:
                file_writer.write(byte_tab)
            images_data.append({
                "uuid": file_uuid,
                "file_path": file_path,
                "has_flash": image['hasFlash']
            })
        for image in images_data:
            self.database_utils.add_image(image['uuid'], image['has_flash'], False, analyse_uuid)
        self.analyse_manager.launch_analyse_thread(analyse_uuid)
        return Response("OK", status=200)

    @route("/createEmptyAccount", methods=["PUT"])
    def create_empty_account(self) -> Response:
        uid = self.get_uuid()
        self.firebase_utils.add_empty_account_document(uid)
        return Response(status=200)

    @route("/account/me", methods=["GET"])
    def get_account(self) -> Response:
        if "x-access-token" not in request.headers:
            print("Token non présent")
            return Response("NOK : Aucun token", status=401)
        token = request.headers["x-access-token"]
        try:
            uid = self.firebase_utils.get_uid(token)
            data = self.firebase_utils.get_account_data(uid)
            data_json = json.dumps(data)
            return Response(data_json, status=200)
        except ValueError:
            print("Token invalide")
            return Response("NOK : Token invalide", status=401)
        except IOError:
            return Response("NOK : Firebase ne marche pas", status=500)

    @route("/account/me", methods=["POST"])
    def set_account_info(self) -> Response:
        if "x-access-token" not in request.headers:
            return Response("NOK : Aucun token", status=401)
        token = request.headers["x-access-token"]
        if not request.headers['Content-Type'].startswith("application/json"):
            return Response("NOK : Le contenu n'est pas du JSON", status=400)
        if 'first_name' not in request.json \
                or 'last_name' not in request.json:
            return Response("NOK : Il manque des champs dans la requête", status=400)
        first_name = request.json['first_name']
        last_name = request.json['last_name']
        try:
            try:
                uid = self.firebase_utils.get_uid(token)
            except ValueError:
                return Response("NOK : Token invalide", status=401)
            self.firebase_utils.set_account_document(uid, first_name, last_name, "user")
            return Response(status=200)
        except IOError:
            return Response("NOK : Firebase ne marche pas", status=500)

    @route("/images/<picture_id>/picture", methods=["GET"])
    def get_image_picture(self, picture_id: str) -> Response:
        user_uuid = self.get_uuid()
        proprietary = self.database_utils.get_user_of_image(picture_id)
        if proprietary is None:
            return Response("NOK : Image non trouvé", status=404)
        elif proprietary != user_uuid:
            return Response("NOK : Cette image ne vous appartient pas", status=401)
        else:
            return send_file(os.path.join(self.working_directory, "images", picture_id + ".jpg"))

    @route("/analyse/<analyse_uuid>", methods=["GET"])
    def get_analyse(self, analyse_uuid: str) -> Response:
        user_uuid = self.get_uuid()
        analyse = self.database_utils.get_analyse_of_uuid(analyse_uuid)
        if analyse is None:
            return Response("NOK : Analyse non trouvé", status=404)
        elif analyse.user_id != user_uuid:
            return Response("NOK : Cette analyse ne vous appartient pas", status=401)
        else:
            return Response(analyse.to_json(), status=200)

    @route("/analyse", methods=["GET"])
    def get_all_analyse(self):
        user_uuid = self.get_uuid()
        analyses = self.database_utils.get_all_analyses_of_user(user_uuid)
        if len(analyses) <= 0:
            result = "[]"
        else:
            result = "["
            for entry in analyses:
                result += entry.to_json()
                result += ","
            result = result[:-1] + "]"
        return Response(result, status=200)

    @route("/analyse/<analyse_uuid>", methods=["DELETE"])
    def remove_analyse(self, analyse_uuid: str):
        user_uuid = self.get_uuid()
        analyse = self.database_utils.get_analyse_of_uuid(analyse_uuid)
        if analyse is None:
            return Response("NOK : Analyse non trouvé", status=404)
        elif analyse.user_id != user_uuid:
            return Response("NOK : Cette analyse ne vous appartient pas", status=401)
        else:
            self.database_utils.remove_analyse(analyse_uuid)
            for image in analyse.images:
                self.database_utils.remove_image(image.uuid)
                os.remove(os.path.join(self.working_directory, "images", image.uuid + ".jpg"))
            return Response(status=200)

    @route("/pieces/<piece_id>/picture", methods=["GET"])
    def get_piece_photo(self, piece_id: str):
        user_uuid = self.get_uuid()
        piece_owner_uuid = self.database_utils.get_user_of_piece(piece_id)
        if piece_owner_uuid is None:
            return Response("NOK : Piece non trouvé", status=404)
        elif piece_owner_uuid != user_uuid:
            return Response("NOK : Cette pièce ne vous appartient pas", status=401)
        else:
            images = self.database_utils.get_all_images_of_piece(piece_id)
            images = sorted(images, key=lambda e:(int(e.with_flash) + int(e.is_dev)))
            if len(images) == 0:
                return Response("NOK : Aucune image/piece trouvé avec cet id", status=400)
            else:
                piece = self.database_utils.get_piece(piece_id)
                image_path = os.path.join(self.working_directory, "images", images[0].uuid + ".jpg")
                image = cv2.imread(image_path)
                croped_image = crop_rectangle_from_image(image, piece.position_x, piece.position_y,
                                                         piece.width, piece.height)
                _, np_byte_array = cv2.imencode(".jpg", croped_image)
                byte_array = np_byte_array.tobytes()
                return Response(byte_array)

    @route("/account/me", methods=["DELETE"])
    def delete_user(self):
        user_uuid = self.get_uuid()
        for analyse in self.database_utils.get_all_analyses_of_user(user_uuid):
            for image in analyse.images:
                self.database_utils.remove_image(image.uuid)
                os.remove(os.path.join(self.working_directory, "images", image.uuid + ".jpg"))
            self.database_utils.remove_analyse(analyse.uuid)
        self.firebase_utils.delete_account_document(user_uuid)
        return Response(status=200)

MainView.register(app, route_base="/")

if __name__ == '__main__':
    app.run()
