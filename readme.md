# AIm Flouze Server

AIm Flouze Server (Analyseur d'Image de Flouze, Money Image Analyser in english) is a REST Server written in Python. The job of this server is to analyse images and search for european coins in it. It also gives a identification system thanks to FireAuth.

## How to use it?

The best way to use AIm Flouze Server is to create a Python Virtual Environment. This Environment shall run Python 3.8 with all packages listed in the requirements.txt.

Once installed, after enabling the VENV, run at the root of this project :

```bash
flask run --host=[IP ADDRESS]
```

## Project structure

### Local modules list

#### classifier

Classifier contains various class that inherit from Classifier. The main role of a Classifier is to classify things.

As such, the most important classifier here is the "MainPieceClassifier" that classify pictures of coins and associate them to a value in cents.

A classifier gives an estimated percentage of certitude to each sample to test.

This module also contains, for the moment, analyse_manager. His role is to launch and manage analysis.

#### database_utils

Database_utils contains only one class for the moment, DatabaseUtils.

Indeed, the server keeps the analysis details on a local SQLite3 server. DatabaseUtils allows the server to communicate with the DataBase through simple functions

#### firebase_utils

Firebase_utils contains also one class only, FireBaseUtils.

To manage authentication and user informations, the server uses FireAuth and FireBase. As such, FireBaseUtils contains tools to verify FireAuth tokens, to get user information...

#### image_utils

image_utils contains a lot of functions to modify images and get informations on images.

It also contains two function to create and use VGGs.

#### models

models contains 3 class. These classes are representation of data stored in the SQLite Database.

#### test

Test contains a bit of unit test scripts. It could be gratly improved.

### Other folders

#### exec_utils

This folder contains two python scripts. Each script creates Maching Learning model. One creates a model that recognizes coins on the head side. The other creates a model that recognizes coins on the head and tail side.

#### output

Output contains the models data created by the two previous mentionel scripts.

#### rsc

rsc contains the used dataset, the key for firebase, and the public/private key used to encrypt user information to firebase.

#### tmp

tmp contains, during execution, the local SQLite3 database, and a folder with all images.

### REST resources list

For EVERY resources, you are excepted to submit in the HTTP request a FireAuth token, associated to your account. It has to be put in the "x-access-token" HTTP header.

#### Account-related resources

- /createEmptyAccount [PUT] : Create an entry in FireBase to stock the user first and last name.
- /account/me 
  - [GET] : Returns the user first and last name, in JSON.
  - [POST] : Updates the user first and last name. The data shall be given in a JSON dict with the "first_name" and "last_name" entry.

#### Analysis-related resources

- /analyse 
  - [GET] : Return all analysis of the user
  - [POST] : Submit a analyse to the user. The data shall be given in a JSON Dict. Check the code for further informations.
- /analyse/<analyse_uuid> 
  - [GET] : Returns all information of a specific analysis, in a JSON Dict.
  - [DELETE] : Remove the analysis from the database

#### Image related resources

- /images/<image_uuid>/picture [GET] : Returns a image based on his uuid
- /piece/<piece_uuid>/picture [GET] : Returns a picture of a given piece based on his id

## Checklist

### State of the project

#### Done

- A user can create an account
- A user can submit an analysis
- A user can retrieve his detailed analysis
- A user can retrieve pictures of his analysis
- A user can delete an analysis
- The servers returns results based on an image

#### To do

- Add more photos to the dataset
- Improve the server analysis of the picture : Right now, the result given by the server is far from perfect. It only bases his result on a dataset that isn't even complete right now. Thus, I have to :
  - Base my result on other methods, such as histogran and radius analysis
  - Change my analysis of the picture to consider the initial picture ratio
  - Allows my analysis to improve the result with a flash AND a flashless picture