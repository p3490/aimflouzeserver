import unittest

import cv2

from image_utils.vgg_utils import create_vgg
from classifier.model_classifier import ModelClassifier
import random
import os
from imutils import paths

THIS_DIR = os.path.dirname(os.path.abspath(__file__))


class ClassifierTestCase(unittest.TestCase):
    def test_model_classifier(self):
        classifier: ModelClassifier = ModelClassifier(os.path.join(THIS_DIR, "..", "output", "model_head_coin.h5"),
                                                      os.path.join(THIS_DIR, "..", "output", "lab_head_coin.pickle"),
                                                      (150, 150, 3))
        image = cv2.imread(os.path.join(THIS_DIR, "1euro.jpg"))
        result = classifier.guess_with_labels(image)
        max_value = 0
        name = ""
        for key in result.keys():
            if max_value < result[key]:
                name = key
                max_value = result[key]
        self.assertEqual("100", name)


if __name__ == '__main__':
    unittest.main()
