import cv2
import os
from classifier.coins.double_circle_classifier import DoubleCircleClassifier
from classifier.coins.piece_data import PieceData

THIS_DIR = os.path.dirname(os.path.abspath(__file__))

if __name__ == '__main__':
    image = cv2.imread(os.path.join(THIS_DIR, "2euro.jpg"))
    double_circle_classifier = DoubleCircleClassifier()
    print(double_circle_classifier.guess(PieceData(image, 0)))
