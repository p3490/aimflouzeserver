import unittest
import cv2
from image_utils.picture_utils import reduce_to_n_colors, get_all_pixels
import os

THIS_DIR = os.path.dirname(os.path.abspath(__file__))


class PictureTestCase(unittest.TestCase):
    def test_reduce_to_n_colors(self):
        # I don't test here if the major color are kept because it
        # would be hard to do
        image = cv2.imread(os.path.join(THIS_DIR, "1euro.jpg"))
        result = reduce_to_n_colors(image, 3)
        pixels = get_all_pixels(result)
        self.assertEqual(3, len(pixels))


if __name__ == '__main__':
    unittest.main()
