import cv2
import os
from image_utils.picture_utils import show_image, get_image_size
import numpy as np
from typing import List, Tuple
from image_utils.picture_utils import get_image_pixel_number

THIS_DIR = os.path.dirname(os.path.abspath(__file__))

if __name__ == '__main__':
    image = cv2.imread(os.path.join(THIS_DIR, "sampleShadow.jpg"))
    show_image(image, size=(500, 500))
    image = cv2.GaussianBlur(image, (15, 15), 0)
    rgb_planes = cv2.split(image)
    result_norm_planes = []
    for plane in rgb_planes:
        dilated_img = cv2.dilate(plane, np.ones((7, 7), np.uint8))
        show_image(dilated_img, size=(500, 500))
        bg_img = cv2.medianBlur(dilated_img, 21)
        diff_img = 255 - cv2.absdiff(plane, bg_img)
        norm_img = cv2.normalize(diff_img, None, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8UC1)
        result_norm_planes.append(norm_img)
    image = cv2.merge(result_norm_planes)

    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    _, image = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    show_image(image, size=(500, 500))
    image = cv2.adaptiveThreshold(image, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 61, 0)
    show_image(image, size=(500, 500))
    count, labels, stats, centroids = cv2.connectedComponentsWithStats(image)
    result: List[Tuple[int, int, int, int, int]] = stats[:, 0:5]
    result = sorted(result, key=lambda item: (item[cv2.CC_STAT_HEIGHT] * item[cv2.CC_STAT_WIDTH]))

    box_size_upper_limit = get_image_pixel_number(image) * 0.3
    while len(result) > 0 \
            and result[-1][cv2.CC_STAT_HEIGHT] * result[-1][cv2.CC_STAT_WIDTH] > box_size_upper_limit:
        del result[-1]

    i = 0
    while i < len(result):
        j = i + 1
        while j < len(result):
            if result[i][cv2.CC_STAT_LEFT] > result[j][cv2.CC_STAT_LEFT] \
                    and result[i][cv2.CC_STAT_TOP] > result[j][cv2.CC_STAT_TOP] \
                    and result[i][cv2.CC_STAT_LEFT] + result[i][cv2.CC_STAT_WIDTH] < result[j][cv2.CC_STAT_LEFT] + result[j][cv2.CC_STAT_WIDTH] \
                    and result[i][cv2.CC_STAT_HEIGHT] + result[i][cv2.CC_STAT_TOP] < result[j][cv2.CC_STAT_HEIGHT] + result[j][cv2.CC_STAT_TOP]:
                del result[i]
                i = 0
                j = i + 1
            else:
                j += 1
        i += 1

    result = list(map(lambda e: (int(e[0]), int(e[1]), int(e[2]), int(e[3]), int(e[4])), result))

    for piece in result:
        print("-----------------------------")
        print((piece[0], piece[1]))
        print((piece[0] + piece[2], piece[1] + piece[3]))
        print("-----------------------------")
        cv2.rectangle(image, (piece[0], piece[1]), (piece[0] + piece[2], piece[1] + piece[3]), (123, 123, 123),
                      thickness=5)
    show_image(image, size=(500, 500))
    print(result)
