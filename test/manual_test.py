import cv2
import os
from image_utils.picture_utils import show_image, get_image_size, reduce_to_n_colors, remove_background
from image_utils.geometry_detection_utils import extract_all_forms, detect_circles, detect_independent_box
from classifier.model_classifier import ModelClassifier
import numpy as np

THIS_DIR = os.path.dirname(os.path.abspath(__file__))

if __name__ == '__main__':
    # image = cv2.imread(os.path.join(THIS_DIR, "sample1.jpg"))
    # show_image(image)
    # image = cv2.GaussianBlur(image, (5, 5), 0)
    # show_image(image)
    # image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # image = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)
    # show_image(image)
    # image = reduce_to_n_colors(image, number_of_colors=2)
    # show_image(image)
    # image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # (T, threshInv) = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)
    # show_image(threshInv)
    # sobelxy = cv2.Sobel(src=threshInv, dx=1, dy=1, ksize=5)
    # show_image(sobelxy)
    # circles = detect_circles(sobelxy, 200, min_radius=24, max_radius=26)
    # for x, y, radius in circles:
    #     cv2.circle(sobelxy, (x, y), radius, (0, 255, 0), 2)
    #     cv2.circle(sobelxy, (x, y), 2, (0, 0, 255), 3)
    # show_image(sobelxy)

    # Fouiller : https://www.techroadmap.in/blog/coin-detection-using-python/
    # Fouiller aussi : https://www.geeksforgeeks.org/find-circles-and-ellipses-in-an-image-using-opencv-python/

    # Lecture de l'image
    image = cv2.imread(os.path.join(THIS_DIR, "sample3.jpg"))
    # show_image(image)

    classifier: ModelClassifier = ModelClassifier(os.path.join("..", "output", "model.h5"),
                                                  os.path.join("..", "output", "lab.pickle"), (150, 150, 3))

    # images, forms = extract_all_forms(image)
    # for piece_image in images:
    #     size = get_image_size(piece_image)
    #     piece_image = cv2.resize(piece_image, (150, 150), interpolation=cv2.INTER_AREA)
    #     show_image(piece_image)
    #     piece_image = reduce_to_n_colors(piece_image, 4)
    #     show_image(piece_image)
    #     circles = detect_circles(piece_image, 200, min_radius=25)
    #     for x, y, radius in circles:
    #         cv2.circle(piece_image, (x, y), radius, (0, 255, 0), 2)
    #         cv2.circle(piece_image, (x, y), 2, (0, 0, 255), 3)
    #     show_image(piece_image)
    image_no_background = remove_background(image)
    cv2.imwrite(os.path.join(THIS_DIR, "testAlpha.png"), image_no_background)