import json


class ImageModel(object):
    """
    An model representing a image submitted

    :param uuid: A unique identifier to this image
    :param with_flash: Was this image taken with a flash?
    :param is_dev: Was this image generated during development?
    """

    def __init__(self, uuid: str, with_flash: bool, is_dev: bool = False):
        self.uuid = uuid
        self.with_flash = with_flash
        self.is_dev = is_dev

    def to_json(self) -> str:
        """
        Returns this model as a String representing a JSON
        :return: A JSON String representative of the model
        """
        return json.dumps(vars(self))
