import json


class PieceModel(object):
    """
    A model representing a piece

    :param value_cents: The value of the piece detected, in cents
    :param position_x: The position of the upper-left corner of this piece, in the X axis
    :param position_y: The position of the upper-left corner of this piece, in the Y axis
    :param width: The width of this piece, in pixels
    :param height: The height of this piece, in pixels
    :param accuracy: A note from 0 to 1 for the accuracy of this piece detection
    :param id: The id of the piece
    """

    def __init__(self, id: int, value_cents: int, position_x: int, position_y: int,
                 width: int, height: int, accuracy: float):
        self.id = id
        self.value_cents = value_cents
        self.position_x = position_x
        self.position_y = position_y
        self.width = width
        self.height = height
        self.accuracy = accuracy

    def to_json(self) -> str:
        """
        Returns this model as a String representing a JSON
        :return: A JSON String representative of the model
        """
        return json.dumps(vars(self))
