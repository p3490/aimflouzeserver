import datetime
from typing import Set
from models.piece_model import PieceModel
from models.image_model import ImageModel
import json


class AnalyseModel(object):
    """
    A submitted analyse

    :param uuid: The unique ID associated to this submitted analyse
    :param user_id: The unique ID associated to the user who submitted this analyse
    :param date_of_post: The date to which this analyse has been submitted
    :param finished: Was this submission finished?
    :param images: A list of images associated to this submission
    :param pieces: A list of pieces that were detected in this image
    :param error: Has the analysis finished with an error?
    """

    def __init__(self, uuid: str, user_id: str, date_of_post: datetime.date, finished: bool,
                 error: bool, images: Set[ImageModel], pieces: Set[PieceModel],):
        self.uuid = uuid
        self.user_id = user_id
        self.date_of_post = date_of_post
        self.finished = finished
        self.images = images
        self.pieces = pieces
        self.error = error

    def to_json(self) -> str:
        """
        Returns this model as a String representing a JSON
        :return: A JSON String representative of the model
        """
        result = vars(self)
        result['images'] = list(map(lambda e: vars(e), result['images']))
        result['pieces'] = list(map(lambda e: vars(e), result['pieces']))
        return json.dumps(result)
