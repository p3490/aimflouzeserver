from keras.preprocessing.image import ImageDataGenerator, img_to_array
from keras.optimizer_v2.adam import Adam
from keras.models import load_model
from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
from image_utils.VGGNet import SmallerVGGNet
import pickle
import cv2
import os
import numpy as np
from typing import List, Any, Tuple, NoReturn

# This files contains functions to create and use VGG Network


def create_vgg(images: List[Tuple[Any, str]], output_folder: str, output_model_name: str = "model.h5",
               output_labels_name: str = "lab.pickle", image_dimensions: Tuple[int, int, int] = (96, 96, 3),
               batch_size: int = 32, init_lr: float = 0.001, epochs: int = 100,
               loss: str ="categorical_crossentropy") -> NoReturn:
    """
    Créer une classifieur CNN (VGGNET) à partir d'un echantillon d'exemple

    :param images: Une liste de tuples composant, dans l'ordre, une image et son label associé
    :pathe dataram output_folder: Le dossier dans lequel sera vidé le résultat
    :param output_model_name: Le nom qui sera donné au modèle
    :param output_labels_name: Le nom qui sera donné au fichier de labels
    :param image_dimensions: Les dimensions dans lesquelles seront traités les images
    :param batch_size: Le nombre d'exemples qui seront utilisés durant une itération de l'entrainement du modèle
    :param init_lr: Le taux d'aprentissage du modèle qui sera généré
    :param epochs: Le nombre d'itérations total pour générer le modèle
    """
    # Créations de nos deux tableaux : Celui d'images, et celui de données
    data = []
    labels = []

    # Remplissage des deux tableaux avec les données reçues
    for image, label in images:
        image = cv2.resize(image, (image_dimensions[1], image_dimensions[0]))
        image = img_to_array(image)
        data.append(image)
        labels.append(label)

    # Adaptations des tableaux en ndarray
    data = np.array(data, dtype="float") / 255.0
    labels = np.array(labels)

    # Binarisation des labels
    lb = LabelBinarizer()
    labels = lb.fit_transform(labels)

    # On sépare les données en deux lots : Ceux pour l'entrainement, et ceux pour les tests
    (trainX, testX, trainY, testY) = train_test_split(data, labels, test_size=0.2, random_state=42)

    # Créations d'un générateur de nouvelles images pour augmenter notre lot de donnée
    # Il ne faut pas en abuser
    datagen = ImageDataGenerator(rotation_range=25, width_shift_range=0, height_shift_range=0,
                                 shear_range=0.2, zoom_range=0, horizontal_flip=False, fill_mode="nearest")
    # datagen = ImageDataGenerator(rotation_range=30, brightness_range=[0.9, 1.1],
    #                              shear_range=0.1, fill_mode="nearest")
    # datagen = ImageDataGenerator(rotation_range=25, width_shift_range=0, height_shift_range=0,
    #                              shear_range=0.2, zoom_range=0, fill_mode="nearest")

    # Initialisation et compilation du réseau
    model = SmallerVGGNet.build(width=image_dimensions[1], height=image_dimensions[0], depth=image_dimensions[2],
                                classes=len(lb.classes_))
    opt = Adam(lr=init_lr, decay=init_lr / epochs)
    model.compile(loss=loss, optimizer=opt, metrics=["accuracy"])

    # Entrainement du réseau
    H = model.fit_generator(
        datagen.flow(trainX, trainY, batch_size=batch_size),
        validation_data=(testX, testY),
        steps_per_epoch=len(trainX) // batch_size,
        epochs=epochs,
        verbose=1
    )

    # Sauvegarde du réseau et des labels
    model.save(os.path.join(output_folder, output_model_name))
    f = open(os.path.join(output_folder, output_labels_name), "wb")
    f.write(pickle.dumps(lb))
    f.close()


def vgg_classify(image: Any, model_path: str, lb_path: str, image_size: Tuple[int, int] = (96, 96)) -> Tuple[str, float]:
    """
    Classifie une image selon un classifieur

    :param image: L'image à tester
    :param model_path: Le chemin vers le modèle du classifieur
    :param lb_path: Le chemin vers la liste de labels du classifieur
    :param image_size: Un tuple contenant la taille horizontale et verticale de l'image
    :return: Un tuple contenant le label associé à l'image, ainsi que son taux de surêté
    """

    image = cv2.resize(image, image_size[0:2])
    image = image.astype("float") / 255.0
    image = img_to_array(image)
    image = np.expand_dims(image, axis=0)

    model = load_model(model_path)
    lb = pickle.loads(open(lb_path, "rb").read())

    proba = model.predict(image)[0]
    idx = np.argmax(proba)
    label = lb.classes_[idx]

    return label, proba[idx] * 100