import imutils
import cv2
from typing import List, Any, Tuple, NoReturn, Set, Dict, Optional
import numpy as np
from sklearn.cluster import KMeans
from math import sqrt

# This file contains useful functions to manipulate and use images


def show_image(image_src: Any, title: str = "Demonstration", size: Optional[Tuple] = None) -> NoReturn:
    """
    Shows an image to the user until he presses the Q button

    :param image_src: The image to show
    :param title: The title of the window
    :param size: To which size the picture has to be seen?
    """
    if size is not None:
        image_src = cv2.resize(image_src, size)
    while True:
        cv2.imshow(title, image_src)
        if cv2.waitKey(50) & 0xFF == ord('q'):
            break


def make_panorama(images_list: List) -> Any:
    """
    Makes a panorama out of several images

    :param images_list: A list of ordonated images
    :return: A panorama, or a Value Error if an error happened
    """
    sticher = cv2.createStitcher() if imutils.is_cv3() else cv2.Stitcher_create()
    (status, result) = sticher.stitch(images_list)
    if status == 0:
        return result
    else:
        raise ValueError("Les images données ne permettent pas de retourner un panorama!")


def get_image_size(image_src: Any) -> Tuple[int, int]:
    """
    Returns the size of a image

    :param image_src: The source image
    :return: A tuple containing in order the width and height of the image
    """
    return int(image_src.shape[1]), int(image_src.shape[0])


def get_image_pixel_number(image_src: Any) -> int:
    """
    Returns the number of pixels in a image

    :param image_src: The source image
    :return: A tuple containing in order the width and height of the image
    """
    return int(image_src.shape[1]) * int(image_src.shape[0])


def crop_image(image_src: Any, pixels_to_remove: int) -> Any:
    """
    Returns an image cropped of n pixel on each sides

    :param image_src: The source image
    :param pixels_to_remove: The number of pixels to remove on each side
    :return: The cropped image
    """
    width_img, height_img = get_image_size(image_src)
    return image_src.copy()[pixels_to_remove:(height_img - pixels_to_remove),
                            pixels_to_remove:(width_img - pixels_to_remove)]


def detect_main_colors(image_src: Any, number_of_colors: int = 1) -> Tuple[np.ndarray, Any]:
    """
    Return the mains color composing an image

    :param image_src: The source image
    :param number_of_colors: The number of colors to detect
    :return: An array containing the peercentage of presence, and another one with the main colors
             present. Theses colors are approximatives
    """
    image_src = cv2.cvtColor(image_src, cv2.COLOR_BGR2RGB)
    image_src = image_src.reshape((image_src.shape[0] * image_src.shape[1], 3))
    clt = KMeans(n_clusters=number_of_colors)
    clt.fit(image_src)
    num_labels = np.arange(0, len(np.unique(clt.labels_)) + 1)
    (hist, _) = np.histogram(clt.labels_, bins=num_labels)
    hist = hist.astype("float")
    hist /= hist.sum()
    return hist, clt.cluster_centers_


def reduce_to_n_colors(image_src: Any, number_of_colors: int = 2) -> Any:
    """
    Reduce a image to a given number of colors

    :param image_src: The source image
    :param number_of_colors: The number of color in the final image
    :return: The source image reduced to a given number of colors
    """
    # Made with this website : https://docs.opencv.org/4.x/d1/d5c/tutorial_py_kmeans_opencv.html
    Z = image_src.reshape((-1, 3))
    Z = np.float32(Z)
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
    ret, label, center = cv2.kmeans(Z, number_of_colors, None, criteria, 10, cv2.KMEANS_RANDOM_CENTERS)

    center = np.uint8(center)
    res = center[label.flatten()]
    res2 = res.reshape(image_src.shape)
    return res2


def reduce_to_n_colors_set(image_src: Any, number_of_colors: int = 2) -> Set:
    """
    Resume the picture to a set of major colors

    :param image_src: The source image
    :param number_of_colors: The number of colors in the final set
    :return: A set of major colors
    """
    reduced_image = reduce_to_n_colors(image_src, number_of_colors)
    return get_all_colors(reduced_image)


def get_all_colors(image_src: Any) -> Set:
    """
    Return all differents colors present in the picture

    :param image_src: The image
    :return: A set containing all colors present in this picture
    """
    w, h = get_image_size(image_src)
    colors = set()
    for x in range(w):
        for y in range(h):
            colors.add(tuple(image_src[y, x]))
    return colors


def get_all_colors_counted(image_src: Any) -> Dict[Any, int]:
    """
    Get all colors in a image, and count them

    :param image_src: The image
    :return: A dictionnay associating to each type of pixel the number of times it appears
    """
    w, h = get_image_size(image_src)
    result: Dict[Any, int] = {}
    for x in range(w):
        for y in range(h):
            if tuple(image_src[y, x]) not in result.keys():
                result[tuple(image_src[y, x])] = 0
            else:
                result[tuple(image_src[y, x])] += 1
    return result


def remove_background(image_src: Any) -> Any:
    """
    Removes the background of a picture.
    The background has to be make of a single, primary, color

    :param image_src: The source image
    :return: A copy of this image with a alpha channel, removing the background
    """
    image_mask = cv2.GaussianBlur(image_src, (15, 15), 0)
    image_mask = reduce_to_n_colors(image_mask, number_of_colors=2)
    color_counts = get_all_colors_counted(image_mask)
    max_color = max(color_counts, key=color_counts.get)
    result = cv2.cvtColor(image_src, cv2.COLOR_BGR2BGRA)
    w, h = get_image_size(image_src)
    for x in range(w):
        for y in range(h):
            if tuple(image_mask[y, x]) == max_color:
                result[y, x, 3] = 0
            else:
                result[y, x, 3] = 255
    return result


def crop_rectangle_from_image(image_src: Any, x: int, y: int, width: int, height: int) -> Any:
    """
    Crop a rectangle from an image.

    :param image_src: The source image
    :param x: The position of the upper-left corner of the rectangle, in the X axis
    :param y: The position of the upper-left corner of the rectangle, in the Y axis
    :param width: The width of the rectangle
    :param height: The height of the rectangle
    :return: The cropped image
    """
    return image_src.copy()[y: y + height, x: x + width]


def get_all_pixels_in_circle(image_src: Any, center_x: int, center_y: int, radius: int) -> List:
    """
    Returns all pixel that can be found in a circle

    :param image_src: The source image
    :param center_x: The position of the center of the circle, in the X axis
    :param center_y: The position of the center of the circle, in the Y axis
    :param radius: The radius of the circle
    :return: The list of pixels that are present in this circle
    """
    result = list()
    for y in range(center_y - radius, center_y + radius):
        for x in range(center_x - radius, center_x + radius):
            if sqrt(pow(center_x - x, 2) + pow(center_y - y, 2)) < radius:
                result.append(image_src[y, x])
    return result


def get_all_pixels_in_donut(image_src: Any, center_x: int, center_y: int, min_radius: int, max_radius: int) -> List:
    """
    Returns all pixels that are present in a donut

    :param image_src: The source image
    :param center_x: The position of the center of the circle, in the X axis
    :param center_y: The position of the center of the circle, in the Y axis
    :param min_radius: The radius of the donut hole
    :param max_radius: The radius of the returns
    :return: The list of pixels that are present in this donut
    """
    result = list()
    for y in range(center_y - max_radius, center_y + max_radius):
        for x in range(center_x - max_radius, center_x + max_radius):
            if min_radius <= sqrt(pow(center_x - x, 2) + pow(center_y - y, 2)) < max_radius:
                    result.append(image_src[y, x])
    return result
