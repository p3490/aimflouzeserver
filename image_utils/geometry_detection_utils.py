import cv2
import numpy as np
from typing import List, Any, Tuple
from .picture_utils import get_image_pixel_number


# This files contains useful functions to detect geometry forms in a picture


def detect_lines_rho_theta(image_src: Any, threshold: int = 230) -> List[Tuple[int, int]]:
    """
    Retourne toutes les lignes détéctés dans une image
    Returns all lignes within a file
           (cf https://opencv24-python-tutorials.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_houghlines/py_houghlines.html )

    :param image_src: The image to analyse
    :param threshold: A arbitary parameters to affine the line searching
    :return: A list of tuples with the rho and theta values of the lines
    """
    gray = cv2.cvtColor(image_src, cv2.COLOR_BGR2GRAY)
    edges = cv2.Canny(gray, 50, 150, apertureSize=3)
    lines = cv2.HoughLines(edges, 1, np.pi / 180, threshold)
    return lines[:, 0]


def detect_lines(image_src: Any, threshold: int = 230, distance: int = 2000) -> List[Tuple[int, int, int, int]]:
    """
    Returns all lignes within a file


    :param image_src: The image to analyse
    :param threshold: A arbitary parameters to affine the line searching
    :param distance: La distance en pixel qui séparera les deux points données
    :return: A list of tules defining the coordinares of two points (x1, y1, x2, y2) that definses the line.
    """
    coordinates = detect_lines_rho_theta(image_src, threshold)

    result = []
    distance /= 2
    for rho, theta in coordinates:
        a = np.cos(theta)
        b = np.sin(theta)
        x0 = a * rho
        y0 = b * rho
        x1 = int(x0 + distance * (-b))
        y1 = int(y0 + distance * a)
        x2 = int(x0 - distance * (-b))
        y2 = int(y0 - distance * a)
        result.append((x1, y1, x2, y2))
    return result


def detect_circles(image_src: Any, min_distance: int,
                   min_radius: int = 0, max_radius: int = 0,
                   param1: int = 50, param2: int = 30) -> List[Tuple[int, int, int]]:
    """
    Detects all circles withing a image

    :param image_src: L'image à traiter
    :param image_src: The souce image
    :param min_distance: The minimal distance between two circles
    :param min_radius: The minimal radius of the circle
    :param max_radius: The maximal radius of the circle (0 if infinite)
    :return: A list of circles defined by tuples with, in the order, the coordinates x and y of their centers, and
             their radius
    """
    image_grey = cv2.cvtColor(image_src, cv2.COLOR_BGR2GRAY)
    image_blur = cv2.medianBlur(image_grey, 5)
    circles = cv2.HoughCircles(image_blur, cv2.HOUGH_GRADIENT, 1, min_distance, param1=param1, param2=param2,
                               minRadius=min_radius, maxRadius=max_radius)
    if circles is not None:
        circles = np.uint16(np.around(circles))
        return circles[0, :, 0:3]
    else:
        return []


def detect_independent_box(image_src: Any, max_peercentage: float = 0.2, min_peercentage: float = 0.002,
                           box_size: int = 61) -> List[
    Tuple[int, int, int, int, int]]:
    """
    Detect all independant forms in a picture and return it to the user

    An indendant form is a form that doesn't contain another form
    :param image_src: The source image
    :param max_peercentage: The maximal peercentage of the image that a box takes
    :return: A list containing in the order, the left point, the top point, the width, the height of the box
             surronding the area, and the area size
    """
    # First step : Removal of shadows in the picture,
    # based on : https://stackoverflow.com/questions/44752240/how-to-remove-shadow-from-scanned-images-using-opencv
    #
    # 1st STEP : Blur + Separation of the image into differents channels
    image = cv2.GaussianBlur(image_src, (15, 15), 0)
    rgb_channels = cv2.split(image)
    # For each channels
    result_norm_planes = []
    for channel in rgb_channels:
        # Dillatation of the image, to accentuate the mains forms
        dilated_img = cv2.dilate(channel, np.ones((7, 7), np.uint8))
        # Blur a second time the image to remove further details
        bg_img = cv2.medianBlur(dilated_img, 21)
        # Separate the mains elements from the background
        diff_img = 255 - cv2.absdiff(channel, bg_img)
        # Normalise the result to have propers values
        norm_img = cv2.normalize(diff_img, None, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8UC1)
        result_norm_planes.append(norm_img)
    # Remerge the channel with each channel
    image = cv2.merge(result_norm_planes)

    # 2nd STEP : Removes the imperfection of the background with a simple OTSU Threshold
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    _, image = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    # 3rd STEP : Mark the mains edges and suppress pieces minor details with a Adaptative Threshold
    image = cv2.adaptiveThreshold(image, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, box_size, 0)

    # Detect all components of this images and sort them by box size
    count, labels, stats, centroids = cv2.connectedComponentsWithStats(image)
    result: List[Tuple[int, int, int, int, int]] = stats[:, 0:5]
    result = sorted(result, key=lambda item: (item[cv2.CC_STAT_HEIGHT] * item[cv2.CC_STAT_WIDTH]))

    # Removes the biggest boxes, based on the max peercentage
    box_size_upper_limit = get_image_pixel_number(image) * max_peercentage
    while len(result) > 0 \
            and result[-1][cv2.CC_STAT_HEIGHT] * result[-1][cv2.CC_STAT_WIDTH] > box_size_upper_limit:
        del result[-1]

    # Removes all boxes that are contained within another box
    i = 0
    while i < len(result):
        j = i + 1
        while j < len(result):
            if result[i][cv2.CC_STAT_LEFT] >= result[j][cv2.CC_STAT_LEFT] \
                    and result[i][cv2.CC_STAT_TOP] >= result[j][cv2.CC_STAT_TOP] \
                    and result[i][cv2.CC_STAT_LEFT] + result[i][cv2.CC_STAT_WIDTH] <= result[j][cv2.CC_STAT_LEFT] + \
                    result[j][cv2.CC_STAT_WIDTH] \
                    and result[i][cv2.CC_STAT_HEIGHT] + result[i][cv2.CC_STAT_TOP] <= result[j][cv2.CC_STAT_HEIGHT] + \
                    result[j][cv2.CC_STAT_TOP]:
                del result[i]
                j = i + 1
            else:
                j += 1
        i += 1

    # Removes the smallest boxes
    box_size_lower_limit = get_image_pixel_number(image) * min_peercentage
    print("LIMIT : " + str(box_size_lower_limit))
    while len(result) > 0 and result[0][cv2.CC_STAT_HEIGHT] * result[0][cv2.CC_STAT_WIDTH] < box_size_lower_limit:
        print("BOX SIZE : " + str(result[0][cv2.CC_STAT_HEIGHT] * result[0][cv2.CC_STAT_WIDTH]))
        del result[0]

    # Returns the result in a simple python list
    result = list(map(lambda e: (int(e[0]), int(e[1]), int(e[2]), int(e[3]), int(e[4])), result))
    return result


def extract_all_forms(image_src: Any, max_peercentage: float = 0.2, min_peercentage: float = 0.002,
                      box_size: int = 61) -> List[Tuple[Any, Tuple[int, int, int, int, int]]]:
    """
    Extracts all the form seen in a image.
    Each form is independent one of another.

    :param image_src: The source image
    :param max_peercentage: The maximum size of a box, relative to the size of the image
    :return: A list of images of each form, and a list containing in the order, the left point, the top point,
             the width, the height of the box surronding the area, and the area size
    """
    forms = detect_independent_box(image_src, max_peercentage, min_peercentage, box_size)
    images: List[Any] = []
    for x, y, w, h, _ in forms:
        images.append(image_src.copy()[y:y + h, x:x + w])
    return list(zip(images, forms))


# def extract_all_commons_forms(images_src: List[Any], max_peercentage: float = 0.2,
#                               box_size: int = 61) -> List[List[Tuple[List[Any],
#                                                                      List[Tuple[int, int, int, int, int]]]]]:
    # main_images, main_forms = extract_all_forms(images_src[0], max_peercentage, box_size)
    # result = [(main_images, main_forms)]
    # for i in range(1, len(images_src)):
    #     supp_images, supp_forms = extract_all_forms(images_src[i], max_peercentage, box_size)
    #     to_add_idxs = find_common_forms(main_forms, supp_forms)
    #     images_to_add = [None] * len(main_images)
    #     forms_to_add = [None] * len(main_forms)
    #     for j in range(0, len(to_add_idxs)):
    #        if to_add_idxs[j] != -1:
    #            images_to_add[to_add_idxs[j]] = supp_images[j]
    #            forms_to_add[to_add_idxs[j]] = supp_forms[j]
    #    result.append((images_to_add, forms_to_add))
    #return result

# def find_common_forms(main_forms: List[Tuple[int, int, int, int, int]],
#                       supp_forms: List[Tuple[int, int, int, int, int]]) -> List[int]:
#     result = [-1] * len(supp_forms)
#     for supp_idx in range(len(supp_forms)):
#         center = (supp_forms[supp_idx][cv2.CC_STAT_LEFT] + (supp_forms[supp_idx][cv2.CC_STAT_WIDTH] / 2),
#                   supp_forms[supp_idx][cv2.CC_STAT_TOP] + (supp_forms[supp_idx][cv2.CC_STAT_HEIGHT] / 2))
#         main_idx = 0
#         while main_idx < len(main_forms) and (
#                 center[0] < main_forms[main_idx][cv2.CC_STAT_LEFT] or
#                 center[0] > main_forms[main_idx][cv2.CC_STAT_LEFT] + main_forms[main_idx][cv2.CC_STAT_WIDTH] or
#                 center[1] < main_forms[main_idx][cv2.CC_STAT_TOP] or
#                 center[1] > main_forms[main_idx][cv2.CC_STAT_TOP] + main_forms[main_idx][cv2.CC_STAT_HEIGHT]
#                 ):
#             main_idx += 1
#         if main_idx < len(main_forms):
#             result[supp_idx] = main_idx
#     return result
