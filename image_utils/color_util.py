from typing import Tuple
from math import sqrt

import cv2
import numpy as np


def get_distance_between_colors(color_a: Tuple, color_b: Tuple) -> float:
    """
    Returns the distance between two colors
    :param color_a: The first color, as a tuple of RGB, BGR...
    :param color_b: The second color, as a tuple of RGB, BGR...
    :return: The euclidian distance between these two colors
    """
    return sqrt(pow(int(color_a[0]) - int(color_b[0]), 2)
                + pow(int(color_a[1]) - int(color_b[1]), 2)
                + pow(int(color_a[2]) - int(color_b[2]), 2))


def get_saturation_value(color: Tuple) -> float:
    """
    Returns the saturation value of an color

    :param color: The first color, as a tuple of BGR
    :return: The saturation value of the color
    """
    img = np.asarray([[color]])
    hsv_img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    return int(hsv_img[0][0][1])
