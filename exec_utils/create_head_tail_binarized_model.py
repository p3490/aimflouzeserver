import cv2
from image_utils.vgg_utils import create_vgg
import random
import os
from imutils import paths

# This executable creates the head/tail model.
# This model can recognize all coins.
# His labels are (P|F)xxx with xxx being the value in cents and P means Tail and F or Face.

THIS_DIR = os.path.dirname(os.path.abspath(__file__))

# A ESSAYER : FAIRE EN BINARISANT POUR AVOIR LES DETAILS

if __name__ == "__main__":
    image_paths = list(paths.list_images(os.path.join(THIS_DIR, "..", "rsc", "datasetPile")))
    image_paths += list(paths.list_images(os.path.join(THIS_DIR, "..", "rsc", "datasetFace")))
    random.seed(42)
    random.shuffle(image_paths)
    image_labels = []
    label_transcription = {
        "01": "200",
        "02": "100",
        "03": "50",
        "04": "20",
        "05": "10",
        "06": "5"
    }
    for path in image_paths:
        image_labels.append((cv2.imread(path), path[-10] + label_transcription[path.split(os.path.sep)[-2]]))
    create_vgg(image_labels, os.path.join(THIS_DIR, "..", "output"), output_model_name="model_head_tail.h5",
               output_labels_name="lab_head_tail.pickle", image_dimensions=(150, 150, 3))
