import cv2
from image_utils.vgg_utils import create_vgg
import random
import os
from imutils import paths

# This executable utils creates the tail coin model.
# This model can only detect tail faces on coins.
# The label associated are the value of the coin, in cents.

THIS_DIR = os.path.dirname(os.path.abspath(__file__))

if __name__ == "__main__":
    image_paths = list(paths.list_images(os.path.join(THIS_DIR, "..", "rsc", "datasetPile")))
    random.seed(42)
    random.shuffle(image_paths)
    image_labels = []
    label_transcription = {
        "01": "200",
        "02": "100",
        "03": "50",
        "04": "20",
        "05": "10",
        "06": "5"
    }
    for path in image_paths:
        image_labels.append((cv2.imread(path), label_transcription[path.split(os.path.sep)[-2]]))
    create_vgg(image_labels, os.path.join(THIS_DIR, "..", "output"), output_model_name="model_tail_coin.h5",
               output_labels_name="lab_tail_coin.pickle", image_dimensions=(150, 150, 3))
